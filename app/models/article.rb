class Article < ApplicationRecord
	#campos
		#id
		#title
		#body
		#visits_count

	#validaciones
	# uniqueness: true valida que esa propiedad no se repita
	# format: {with: /expresion_regular/}
	validates :title, presence: true
	validates :body, presence:true, length: { minimum: 20}
end
