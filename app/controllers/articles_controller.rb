class ArticlesController < ApplicationController

	#ejemplos métodos active records básico
	# all = obtiene todos los registros
	# find = encuentra registro por su id
	# all.count = el número de registros
	# find_by (atributo: "dato") = el regsitro que coincidan con la busqueda, siempre retorna uno
	# where ("title LIKE ?", "%lo_que_quieres_que_coincida%") = reemplazar variables por signo de interrgación, después de la coma, devuelve más de uno
	# where ("id = ? OR title = ?", params[:id], params[:titile])
	# where.not ("title LIKE ?", "%lo_que_quieres_que_coincida%") = trae los que no coinciden con lo que estás buscando
	# destroy = elimna el objeto de la base de datos
	# @article.update_attibutes({titile: 'Nuevo título'})


	# GET /articles
	def index
		#trae todos los registros de la base de datos
		# de la tabla article
		@articles = Article.all
	end

	# GET /articles/:id
	def show
		#params es un hash con todo los parametros que se mandaron al servidor web
		@article = Article.find(params[:id])
	end

	#Get /articles/new
	def new
		@article = Article.new
	end

	#POST /articles
	def create
		@article = Article.new(article_params)

		# esto funciona pero pudiera llegar a ser inseguro (inyección de código), por lo que se usará strong params
		# @article = Article.new(title: params[:article][:title], 
		# 						body: params[:article][:body])

		# para validarque se cumplieron las condiciones del modelo
		# @article.valid?
		# @article.invalid?
		if @article.save
			redirect_to @article
		else
			render :new
		end
	end

	# get "/articles/:id/edit"
	def edit
		@article = Article.find(params[:id])
	end

	# put "/articles/:id"
	def update
		@article = Article.find(params[:id])

		if @article.update(article_params)
			redirect_to @article
		else
			render :edit
		end
	end

	#DELETE /articles:id
	def destroy
		@article = Article.find(params[:id])
		@article.destroy # elimina el objeto de la base de datos
		redirect_to articles_path
	end

	private
	#strong params
	def article_params
		params.require(:article).permit(:title,:body)
	end

end