class WelcomeController < ApplicationController
  #para hacer que todas las acciones del controller usen el mismo layout
  #render layout: 'otherLayout'

  def index
  end

  # para hacer que una acción especifica use otro layout
  # def otherAction
  # 	render layout: 'otherLayout'
  # end
end
