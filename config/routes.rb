Rails.application.routes.draw do
  devise_for :users
  #get 'welcome/index'

  #poner rutas especiales
  #get 'especial', to: 'welcome#index'

  resources :articles #controlador RESTFUL (rutas y acciones definidiadas)
  #except: [:delete]
  #only: [:delete]

  #es lo mismo qué
  	# get "/articles" 			index
  	# post "/articles" 			create
  	# delete "/articles:id"  destroy
  	# get "articles/:id" 		show
  	# get "/articles/new" 		new
  	# get "/articles/:id/edit" 	edit
  	# patch "/articles/:id" 	update
  	# put "/articles/:id"  		update
  

  root 'welcome#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
